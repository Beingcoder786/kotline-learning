fun task() {
    var m = mutableMapOf<String, Int>("rice" to 50, "pulse" to 60, "butter" to 70)

    println(m)                   //print map

    m["oil"]=70                  ////add item
    println(m)                        //print again
    m.remove("butter")           //remove single item
    println(m)                   // print

    //for((key,value) in m)
      //   m.remove($key)
   // println(m)
    println("all item -")         //print all along price

    for ((k,v) in m)   //print all
        println("$k=$v")

    /*m.forEach { k, v ->
        println("$k = $v")
    }*/

    var sum:Int=0               //sum of values.
    for((k,v) in m)
    {
       sum+=m.getValue(k)
    }
    println("sum is $sum")

    val itr = m.entries.iterator()   //remove all using iterator
    while (itr.hasNext())
    {
        //val curr = itr.next()
        itr.next()
            itr.remove()


    }
    println(m)






}
fun main()
{
    task()
}
