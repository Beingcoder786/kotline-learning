
import java.util.*

// OOPS PRACTICE
////////////////////////////////////////////////////////////////////////////////
class Student()                                                 //SimpleClass
{
    //variable
    var name="durgesh"
    var age=23


    //method

    fun Display()
    {
        println("my name is ${name} ans ${age}")
    }

}

/////////////////////////////////////////////////////////////////////////////

class Animal(val name:String,val color:String)             //primary constructor
{
    //if we want to this  name put in another
    // variable than use init
    val animalname:String
    init {                                                 //INIT BLOCK
        animalname=name
        println("initializer block")


    }
   fun display()
   {
       print("${name}  and ${color}")
       println()
   }

}
////////////////////////////////////////////////////////////////////////////////

class Apple                                           //SECONDARY CONSTRUCTOR
{
    val color:String
    val price:Int
    constructor(color: String,price: Int)
    {
        this.color=color
        this.price=price

    }
    fun DisplayApple()
    {
        println("Apple are like ${color} and ${price}")
    }
}
/////////////////////////////////////////////////////////////////////////////////////////

open class person()                                       //inheritence practice
{
    var id:Int=123
    open fun DisplayPersonID()
    {
        println("person id is ${id}")
    }
}

class emp:person() {

    override fun DisplayPersonID()                       //overring function
    {
        println("child function is running")
    }
}
//////////////////////////////////////////////////////////

abstract class Ample()                         //abstract class demo
{
    abstract fun unimplemt()
    fun hello()
    {
        println("hello this is complete function")
    }

}
class testt:Ample()
{
    override fun unimplemt() {
        println("now this method is implemented")
    }
}
 ////////////////////////////////////////
   interface  myinterface                                  //interface
   {
       fun abs()

   }
  class myinterim:myinterface
  {
      override fun abs() {
          print("implmented interface")
      }
  }

 fun Exceptionn()
 {
     println("try with catch")
     var obj=Scanner(System.`in`)
     try {
         println("enter value of a -           ")
         var a = obj.nextInt()
         println("enter value of b -         ")
         var b = obj.nextInt()
         var c = a / b
         print(c)
     }
     catch (e:ArithmeticException)
     {
         print("exception occured ${e.message}")
     }

 }
////////////////////////////////////////////////////////////////////////////
fun describeString(maybeString: String?): String {               //null safety check
    if (maybeString != null && maybeString.length > 0) {
        return "String of length ${maybeString.length}"
    } else {
        return "Empty or null string"
    }
}

/////////////////////////////////////////////////////////////////////////////////
fun main(args: Array<String>) {                             //MAIN FUNCTION ENTRY POINT
 var obj1=Student()
    obj1.Display()

    //change name and age. just like override
    obj1.name="ABhi"
    obj1.age=20
    obj1.Display();

    var animalobj=Animal("deer","black")         //primary consturctor
    print("${animalobj.name}  ")
    println(animalobj.color)
    //we can use function for multiple object like

    animalobj.display();
    var animalobj2=Animal("dog","white")//primary consturtor
    animalobj2.display()


                                           //secondary constuctor call
    var Appleobj=Apple("red",200)
    Appleobj.DisplayApple()


    var empobj=emp()               //inheritence
    empobj.DisplayPersonID()

    var testtobj=testt()          //abstract wala
    testtobj.unimplemt()
    testtobj.hello()


    var interobj=myinterim()                               //interface implemet
    interobj.abs()


    Exceptionn()

    val lambda={x:Int,y:Int->x+y}                            //lambda function

     println(lambda(1,2))

    val sayhi={msg:String-> println("hello $msg")}
     sayhi("ABi")

    println(describeString("abhishek"))  //null safety
    println(describeString(null))


}

