val Users: MutableList<Int> = mutableListOf(1, 2, 3)   // list Example

fun addUser(newUser: Int) {   //add item in mutable list.
    Users.add(newUser)
}

fun sett()      ////set example
{
    val openIssues: MutableSet<String> = mutableSetOf("red", "violet", "red") // 1
    openIssues.add("pink")

    println(openIssues)
}

fun usefind() {
    val words = listOf("Lets", "find", "something", "in", "collection", "somehow")

    val first = words.find {
        it.startsWith("some")
    }
    val last = words.findLast {
        it.startsWith("some") }
    val nothing = words.find { it.contains("nothing") }
    println(first)
    println(last)
    println(nothing)

    val numbers = listOf(1, -2, 3, -4, 5, -6)

    val firstt = numbers.first()
    val lastt = numbers.last()

    val firstEven = numbers.first { it % 2 == 0 }
    val lastOdd = numbers.last { it % 2 != 0 }
    println("first even is $firstEven and last odd is $lastOdd")
}

fun mapp() {

    val Employee: MutableMap<Int, Int> = mutableMapOf(1 to 100, 2 to 200, 3 to 300, 6 to 430)
    println(Employee)
    Employee[4] = 500
    Employee[5] = 400
    println(Employee)
    println(Employee)
    if (Employee.containsKey(2))
        println("yes this is exist")

    val m: MutableMap<Int, String> = mutableMapOf(1 to "Abhi", 2 to "Jack", 3 to "Bhishm")

    for ((k, v) in m)    //one way
        println("$k=$v")


    for (item in m)   //another way
        println(item)
    //println(m);
}

fun main(args: Array<String>) {
    addUser(4)   //List collection.


    sett() //practice set Collection.
    mapp() //practice mapp Collection.

    usefind();
}
