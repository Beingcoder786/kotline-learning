import java.lang.Integer.parseInt
import java.util.*

fun filterr()                        //exrecise of filter
{
    val deco= listOf("Red","blue","Reel","Insta")
      println(deco.filter { true })
    println(deco.filter { it[0]=='R' })
    println(deco.filter { it[2]=='e' })
    //println(deco.filter { it[3]=='I' &&it[1]=='b' })
}

fun whatShouldIDoToday(mood: String, weather: String = "sunny", temperature: Int = 24) : String {
    return when {
        mood == "happy" && weather == "sunny" -> "go for a walk"
        else -> "Stay home and read."
    }
}
fun swim(name:String="fast")    //  default parameter
{
    println(name)
}
fun dayofweek()
{
   // println("today is WednesDay")
    println(Calendar.getInstance().get(Calendar.DAY_OF_WEEK) )
    val day = Calendar.getInstance().get(Calendar.DAY_OF_WEEK)
    println( when (day) {
        1 -> "Sunday"
        2 -> "Monday"
        3 -> "Tuesday"
        4 -> "Wednesday"
        5 -> "Thursday"
        6 -> "Friday"
        7 -> "Saturday"
        else -> "Time has stopped"
    })
}



fun printt()
{
    println("ha ho gya")
}
fun arrayy()
{

    val id = arrayOf(1,2,3,4,5)///array declaration
    val firstId = id[0]
    println(firstId);
    for(item in id)
         println(item)
    for(item in 0..4)
        println(item)
}

fun raw()
{
    val text1 ="""        
                 Welcome   
                     To  
               ZopSmart 
        """
    println(text1)

}

fun conversion()
{

    //Type conversion
    var value1 = 100
    val value2: Long =value1.toLong()
    println("converted value is $value2")
}
fun summ(a: Int, b: Int): Int
{
    return a+b
}

fun printProduct(arg1: String, arg2: String) {
    val x = parseInt(arg1)
    val y = parseInt(arg2)

    // Using `x * y` yields error because they may hold nulls.
    if (x != null && y != null) {
        // x and y are automatically cast to non-nullable after null check
        println(x * y)
    }
    else {
        println("'$arg1' or '$arg2' is not a number")
    }
}
fun ifelse()
{
    val num1 = 10
    val num2 =20
    val result = if (num1 > num2) {
        "$num1 is greater than $num2"
    } else {
        "$num1 is smaller than $num2"
    }
    println(result)
}

fun feedthefish()
{
    val day="Thursday"
    val food=option(day)
     println("day is $day and food is $food")
}
fun option(day:String):String
{
    var food="fasting"
    when(day)
    {
        "monday"->food="poha"

        "Tuesday"->food="biryani"

        "Wednesday"->food="idli"

        "Thursday"->food="poha"

        "friday"->food="dosa"

        "saturday"->food="poha"

          else ->"fasting"
    }
    return food;

}

fun getFortuneCookie() : String {
    val fortunes = listOf( "You will have a great day!",
        "Things will go well for you today.",
        "Enjoy a wonderful day of success.",
        "Be humble and all will turn out well.",
        "Today is a good day for exercising restraint.",
        "Take it easy and enjoy life!",
        "Treasure your friends, because they are your greatest fortune.")
    print("\nEnter your birthday: ")
    val birthday = readLine()?.toIntOrNull() ?: 1   //for take input.
    return fortunes[birthday.rem(fortunes.size)]
}
fun main(args: Array<String>){      //main function

       printt()  //simple print program
       arrayy();   //array decalration
        raw();     //manual pattern string
       conversion();// type conversion in kotlin
      dayofweek();
       feedthefish();

     swim();  //try for default parameter.
    filterr()
    println(whatShouldIDoToday("happy"))

    println("\nYour fortune is: ${getFortuneCookie()}")


      println("addition is ${summ(10,20)}");  //function declaration.
       printProduct("20","32");

     ifelse();

    var number = 4
    var numberProvided = when(number) {
        1 -> "One"
        2 -> "Two"
        3 -> "Three"
        4 -> "Four"
        5 -> "Five"
        else -> "invalid number"
    }
    println("You provide $numberProvided")

}


